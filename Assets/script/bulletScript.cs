﻿using UnityEngine;
using System.Collections;

public class bulletScript : MonoBehaviour {
	public int type; 
	public int Dmg;
	float lifetime = 1f;
	public bool original = true;

	
	// Lodes uzvedības scripts
	void Update () 
	{

		//Iznīcina lodi pēc notiekta laika
		if (!original) 
		{
			lifetime -= Time.deltaTime;
			if (lifetime < 0)
				Destroy (gameObject);

		}
	}
	void OnTriggerEnter2D(Collider2D col)
	{	
		if (col.tag == "creep") 
		{
			DoDmg (col.gameObject.GetComponent<creepscript> ());

		}

	}
	//Izpilda lodes efektu ( ko nosaka type) tam creepam, ar ko detektēta sadrusme
	void DoDmg(creepscript creep)
	{
		//Parasts DMG
		if (type == 0) 
		{
			creep.hp -= Dmg;
			Destroy (gameObject);
		}
		//Slow
		if (type == 1) 
		{
			if (creep.speed == creep.constSpeed) 
			{
				creep.speed = creep.speed * 0.6f;
				creep.timeLeft = 3;
			}
			Destroy (gameObject);
		}
		//AoE 
		if (type == 2)
		{
			Collider2D[] colliders = Physics2D.OverlapCircleAll (new Vector2(transform.position.x, transform.position.y), 3);
			foreach (Collider2D enemy in colliders)
			{
				if (enemy.gameObject.tag == "creep")
				enemy.gameObject.GetComponent<creepscript> ().hp -= Dmg;
			}
			Destroy (gameObject);
		}
		//AoE slow
		if (type == 3) 
		{
			if (creep.speed == creep.constSpeed)
			{

				Collider2D[] colliders = Physics2D.OverlapCircleAll (new Vector2(transform.position.x, transform.position.y), 3);
				foreach (Collider2D enemy in colliders)
				{
					if (enemy.gameObject.tag == "creep")
						enemy.gameObject.GetComponent<creepscript> ().speed *= 0.6f;
					try
					{
					enemy.gameObject.GetComponent<creepscript> ().timeLeft= 3;
					}catch{

					}
				}
			}
			Destroy (gameObject);
		}
	}
		
}
