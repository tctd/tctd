﻿using UnityEngine;
using System.Collections;

public class creepscript : MonoBehaviour {
	public Transform[] points;
	public Transform end;
	public double speed;
	public int hp;
	public int player;
	public double constSpeed;
	public float timeLeft = 3;
	int i =0;
	public AudioSource source;
	public AudioClip death;
	// Creepu uzvedības loģika


	void Start () 
	{
		// Inicializē creepa kustēšanos
		speed = constSpeed;
		if (transform.position.x < 100) 
		{
			speed += variables.BonusSpeed [player];
			constSpeed = speed;
			hp += variables.bonusHP [player];
		}
	}


	void Update () 
	{
		// Nomirst un atņem dzīvību, ja tiek līdz ceļa galam
		if (Vector3.Distance (transform.position, end.position) < 0.2f) 
		{
			variables.lives [player]--;
			Death ();
		}
		//Kustēšanās
		try
		{
			transform.position = Vector3.MoveTowards (transform.position, points [i].position,(float)speed*Time.deltaTime);
		
		//Pāriet uz nākamo mērķa checkpointu, ja ir tuvu šī brīža checkpoint
			if (Vector3.Distance (transform.position, points [i].position) < 0.2f) 
				i++;
				Vector3 diff = points [i].position- transform.position;
				diff.Normalize ();
				float rotz = Mathf.Atan2 (diff.y, diff.x) * Mathf.Rad2Deg-90;
				float cur = transform.rotation.eulerAngles.z;
				float lerped = Mathf.LerpAngle(rotz,cur,0.85f);
				transform.rotation = Quaternion.Euler (0, 0, lerped);

		}catch{
		}

		if (hp < 1) 
			Death ();

		//Timing loģika, lai noņemtu slow efektu
		if (speed != constSpeed)
		{
			if (timeLeft > 0) 
			{
					timeLeft -= Time.deltaTime;
			} 
		}
		if (timeLeft <= 0) 
		{
			speed = constSpeed;
		}
	}

	//Nāves loģika
	void Death()
	{
		source.PlayOneShot (death);
		variables.creepcount[player]--;
		Destroy (gameObject);

	}
}
