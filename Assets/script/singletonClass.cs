﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class singletonClass : MonoBehaviour 
{
	
	public GameObject GameMessage;
	public Text GameMessageText;
	public GameObject GameMessage2;
	public Text GameMessageText2;

	void Awake () 
	{
		variables.Singleton = this;
	}
	
	// Update is called once per frame

	public void ShowMessage(string text)
	{
		GameMessage.SetActive (true);
		GameMessageText.text = text;
		StartCoroutine (HideMsg (1));

	}
	public void pickPlayer(int player,string text)
	{
		if (player == 0)
			ShowMessage (text);
		if (player == 1)
			ShowMessage2 (text);

	}
	public void ShowMessage2(string text)
	{
		if (variables.WithAI)
			return;
		GameMessage2.SetActive (true);
		GameMessageText2.text = text;
		StartCoroutine (HideMsg (2));
	}
	IEnumerator HideMsg(int MessageNumber)
	{
		yield return new WaitForSeconds (1.5f);
		try
		{
		if(MessageNumber ==1)
			GameMessage.SetActive (false);
		if(MessageNumber ==2)
			GameMessage2.SetActive (false);
		}
		catch {
			Debug.Log ("problem with message system");
		}

	}
	}
