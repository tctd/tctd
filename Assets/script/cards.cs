﻿using UnityEngine;
using System.Collections;

public class cards : MonoBehaviour {
	public static ArrayList PersistentDeck1 = new ArrayList();
	public static ArrayList gameDeck1 = new ArrayList();
	public ArrayList deck2 = new ArrayList();
	public static ArrayList cards1 = new ArrayList();
	public static ArrayList cards2 = new ArrayList();
	public static ArrayList[] hands = { new ArrayList (), new ArrayList () };
	public GameObject cardTemp;
	public GameObject cardTempAction;
	public GameObject cardTempBack;
	public Transform[] handPos;
	public static bool doOrg= false;
	public Transform[,] AiPos;
	// Galvenais karšu loģikas scripts

	//Ierakstam visu kāršu datus
	public void Start () 
	{
		hands [0].Clear ();
		hands [1].Clear ();
		for (int i = 0; i < PersistentDeck1.Count; i++) 
		{
			gameDeck1.Add (PersistentDeck1[i]);
		}



		//(string Name, string Desc, int Cost, int dmg, int reloadspeed, int Range,int Type)
		/*cards.cards1.Add (new cards.TowerCardData ("Meatball cannon", "Basic",1 , 2, 2, 6, 0,true));
		cards.cards1.Add (new cards.TowerCardData ("Pasta tower", "Basic 2", 2, 3, 2, 8, 1,true));
		cards.cards1.Add (new cards.TowerCardData ("Bologna sauce tower", "AoE", 2, 2, 1, 6, 2,true));
		cards.cards1.Add (new cards.TowerCardData ("Macaroni shooter", "Sniper", 3, 4, 1, 10, 3,true));
		cards.cards1.Add (new cards.TowerCardData ("Melted cheese canon", "AoE Slow", 2, 0, 1, 6, 4,true));
		cards.cards1.Add (new cards.TowerCardData ("Beer volcano", "Slow", 2, 1, 2, 7, 5,true));
		cards.cards1.Add (new cards.TowerCardData ("Canon", "Damage", 3, 6, 1, 4, 6,true));
		cards.cards1.Add (new cards.TowerCardData ("His holy \nnoodleness shrine", "Percent damage", 2, 1, 3, 6, 7,true));
		cards.cards1.Add (new cards.TowerCardData ("Instant noodles", "Rapid fire canon", 2, 1, 4, 6, 8,true));
		cards.cards1.Add (new cards.TowerCardData ("Blessings of the holy spirits", "Your cultists \nbecome more \ndurable", 1, 0, 0, 0, 9,false));
		cards.cards1.Add (new cards.TowerCardData ("Sacrifice", "His savoury majesty\n transports a\n follower\nto the circle", 3,  0, 0, 0, 10,false));
		cards.cards1.Add (new cards.TowerCardData ("Touched by his noodelieness", "His touch gives\n you haste", 1,  0, 0, 0, 11,false));
		cards.cards1.Add (new cards.TowerCardData ("The first midget", "\"..and on the first\n day he created\n the Earth, Heavens\n and \na midget", 3, 0, 0, 0, 12,false));
		*/


			Deal ();


	}
	//saliekam kārtis spēlētāju deckos
	public void Deal()
	{
		

		/*foreach (TowerCardData card in cards1) {
			for (int i = 0; i < 4; i++) {
				deck1.Add (card);
			}
		}*/
		foreach (TowerCardData card in cards2) 
		{
			for (int i = 0; i < 4; i++) 
			{
				deck2.Add (card);
			}
		}

		Draw (5);

	}
	// izvilkt kārti no deck
	public void Draw(int howMuch)
	{
		//deck1
		for(int i=0;i<howMuch;i++)
		{
			if (gameDeck1.Count > 0) 
			{
				if (hands [0].Count < 6) 
				{
					int whichcard = Random.Range (0, gameDeck1.Count - 1);
					TowerCardData card = (TowerCardData)gameDeck1 [whichcard];
					Createcard (card, 0);
					gameDeck1.RemoveAt (whichcard);
				}else {
					variables.Singleton.ShowMessage("You have too many cards in hand!");
				}

			} else {
				variables.Singleton.ShowMessage("You have no more cards in the deck!");
			}

			//deck2
			if (deck2.Count > 0) 
			{
				if (hands [1].Count < 6) 
				{
					int whichcard = Random.Range (0, deck2.Count - 1);
					TowerCardData card = (TowerCardData)deck2 [whichcard];
					Createcard (card, 1);	
					deck2.RemoveAt (whichcard);	
				} else {
					variables.Singleton.ShowMessage2("You have too many cards in hand!");
				}

			} else {
				variables.Singleton.ShowMessage2("You have no more cards in the deck!");
			}
			organize ();
		}
		AI.doMove = true;
	}
	// Izveidot kārts instanci
	public void Createcard(TowerCardData card,int player)
	{
		GameObject cardInst = null;
		// Izveidojam kārti no atbilstošā template (Action vai tower)
		if (variables.WithAI && player == 1) 
		{
			cardInst = (GameObject)Instantiate (cardTempBack, handPos [player].position, Quaternion.Euler (new Vector3 (0, 0, 180 * player)));	


		} else {
			if (card.isTower) 
			{
				cardInst = (GameObject)Instantiate (cardTemp, handPos [player].position, Quaternion.Euler (new Vector3 (0, 0, 180 * player)));	
				cardInst.GetComponent<cardisntance> ().SetText ("Damage: " + card.damage.ToString ()
				+ "\nRange: " + card.range + "\nCost: " + card.cost + "\n" + card.description);
			} else 
			{
				cardInst = (GameObject)Instantiate (cardTempAction, handPos [player].position, Quaternion.Euler (new Vector3 (0, 0, 180 * player)));	
				cardInst.GetComponent<cardisntance> ().SetText (card.description + "\nCost: " + card.cost);
			}
		}
		// Iestatam kārts datus
		cardisntance cardScript = cardInst.GetComponent<cardisntance> ();
		cardScript.type = card.type;
		cardScript.player = (player+1)%2;
		cardScript.cost = card.cost;
		cardScript.isTower = card.isTower;
		cardInst.GetComponentInChildren<cardisntance> ().SetImage ();
		hands [player].Add (cardInst);

	}


	void Update () 
	{
		// Sakārto kārtis rokā, ja kāds cits scripts ir no mainijis doOrg mainīgo
		if (doOrg) 
		{
			doOrg = false;
			organize ();
		}
	}

	// Sakārto kārtis rokā, lai nebūtu tukšums kad kādu izspēlē un lai jaunās kārtis ir pareizās vietās
	public void organize()
	{
		for(int j=0;j<2;j++) 
		{
			for (int i=0; i<hands[j].Count; i++) 
			{
				GameObject temp = (GameObject)hands[j][i];
				Vector3 v = temp.transform.position;
				v.x = handPos[j].position.x + hands[j].Count - 1.5f * i+1.5f;
				temp.transform.position = v;
				if (j == 0) 
				{
					temp.GetComponent<SpriteRenderer> ().sortingOrder = -i * 3 - 1;
					temp.GetComponent<cardisntance> ().SetOrder (-i*3 - 1);
				} 
				else 
				{
					temp.GetComponent<SpriteRenderer> ().sortingOrder = -100+i * 3 ;
					temp.GetComponent<cardisntance> ().SetOrder ( -100+i * 3 );
				}

			}
		}



}

	// Datu objekta klase. Tas instancēs glabājas katras kārts dati
public class TowerCardData : MonoBehaviour {
	public string Name;
	public string description;
	public int cost;
	public int damage;
	public int fireRate;
	public int range;
	public int type;
		public bool isTower;
		public TowerCardData(string Name, string Desc, int Cost, int dmg, int reloadspeed, int Range,int Type,bool IsTower){
		isTower = IsTower;
		this.Name = Name;
		description = Desc;
		cost = Cost;
		damage = dmg;
		fireRate = reloadspeed;
		range = Range;
		type = Type;

	}
}
}