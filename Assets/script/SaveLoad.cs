﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
public static class SaveLoad {

	public static ArrayList list1 = new ArrayList();



	public static void Save(ArrayList myList1) {
		SaveLoad.list1=myList1;
		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Create (Application.persistentDataPath + "/savedGames.gd"); 
		bf.Serialize(file, SaveLoad.list1);
		file.Close();
	}   

	public static void Load() {
		if(File.Exists(Application.persistentDataPath + "/savedGames.gd")) {
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open(Application.persistentDataPath + "/savedGames.gd", FileMode.Open);
			Debug.Log (Application.persistentDataPath + "/savedGames.gd");
			SaveLoad.list1 = (ArrayList)bf.Deserialize(file);
			//SaveLoad.list1 = null;
			file.Close();
		}
	}
}
