﻿using UnityEngine;
using System.Collections;

public class spawner : MonoBehaviour {
	public GameObject[] creeps;
	public GameObject[] creepsStrong;
	public GameObject[] cardCreeps;
	public Transform[] starts;
	guiman guiManager;

	//Creep spawning logic
	void Start () 
	{		
		guiManager = GetComponent<guiman> ();
	}

	//Izsauc pirms katras spawnošanas
	public void StartSp()
	{
		guiManager.SetCreepCount (0, 0);
		guiManager.SetCreepCount (1, 0);
		StartCoroutine ("Spawn");
	}

	//Spawnošanas loģika
	IEnumerator Spawn()
	{
		//Atrod maksimālo pieejamo creepu skaitu
		int loopcount =(int)Mathf.Max ((float)(variables.cultistCount [0] + variables.cardBeasts [0]),
		(float)(variables.cultistCount [1] + variables.cardBeasts [1]));
		loopcount+=2;
		//Spawno atbilstošo tipu creepus abiem spēlētājiem
		for (int i = 1; i < loopcount; i++) 
		{
			for (int j = 0; j < 2; j++) 
			{
				if (i <= variables.cardBeasts [(j + 1) % 2]) 
				{
					Instantiate (cardCreeps [j], starts[j].position, Quaternion.identity);
					variables.creepcount [(j + 1) % 2]++;
				} 
				else 
				{
					if ((i<variables.level+variables.cardBeasts [(j + 1) % 2]-1) &&
						i <= (variables.cultistCount [(j + 1) % 2] + variables.cardBeasts [(j + 1) % 2])) 
					{
						Instantiate (creepsStrong [j], starts [j].position, Quaternion.identity);
						variables.creepcount [(j + 1) % 2]++;
					} 
					else 
					{
						if (i <= (variables.cultistCount [(j + 1) % 2] + variables.cardBeasts [(j + 1) % 2])) 
						{
							Instantiate (creeps [j], starts [j].position, Quaternion.identity);
							variables.creepcount [(j + 1) % 2]++;
						}
					}
				}
			}
			variables.battlephase = true;
			yield return new WaitForSeconds (0.6f);
		}
		variables.battlephase = true;
		//Nonullēt pieejamo creepu skaitītāju
		variables.cultistCount [0] = 0;
		variables.cultistCount [1] = 0;
		GetComponent<guiman> ().SetCreepCount (0, 0);
		GetComponent<guiman> ().SetCreepCount (1, 0);
	}

}
