﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class deckbuild : MonoBehaviour {
	public Transform start;
	public static int currentCardCount = 0;
	public GameObject cardTemp;
	public GameObject cardTempAction;
	public Text deckcount;
	public GameObject deckthing;
	public RectTransform content;
	ArrayList currentCards = new ArrayList ();

	public static ArrayList deckthings = new ArrayList ();
	int currentpage=0;
	//Deckbuilding loģika-kāršu ielikšana deckā no spēles


	void Start () 
	{
		//saglabato decku no diska

		//Izveidot objektu, kurā glabāsies informācija par to, kādas kārtis šobrīd ir deckā
		while (deckinitialize.cardsinDeck.Count < cards.cards1.Count) 
			deckinitialize.cardsinDeck.Add (0);
			
			InitializePage ();
			InitializeDeck ();

	}

	//Ielādē šobrīd deckā esošās kārtis parādīšanai un manipulācijai
	void InitializeDeck()
	{
		for (int j = 0; j < deckinitialize.cardsinDeck.Count; j++) 
		{
			try
			{
				cards.TowerCardData card = (cards.TowerCardData)cards.cards1[j];
				for (int i = 0; i < (int)deckinitialize.cardsinDeck[j]; i++) 
				{				
					if (i == 1) 
					{
					//Vector3 pos = new Vector3 (content.position.x, content.position.y- deckbuild.currentCardCount * 12+100, 0);

					GameObject cardInst = (GameObject)Instantiate (deckthing, content.position, Quaternion.Euler (new Vector3 (0, 0, 0)));
					cardInst.transform.SetParent (content);
					Vector3 pos = new Vector3 (content.position.x, content.position.y - deckbuild.currentCardCount * 50 + 100, 0);
					content.position = pos;
					//content.sizeDelta =new Vector2(100, 50 * deckbuild.currentCardCount);
					deckbuild.currentCardCount++;
					deckThing thing = cardInst.GetComponent<deckThing> ();
					thing.type = card.type;
					thing.name = card.Name;
					}
				}
			}
			catch
			{			
				Debug.Log ("Som Ting Wong "+j);
			}
		}
	}

	//Izveido pirmo lapu ar kārtīm ko parādīt
	void InitializePage()
	{
		if ((currentpage - 1) * 6 < cards.cards1.Count) 
		{			
			GameObject cardInst = null;
			for (int j = 0; j < 2; j++) 
			{
				for (int i = 0; i < 3; i++) 
				{
					if (i + j * 3 + currentpage * 6 < cards.cards1.Count)
					{
						cards.TowerCardData card = (cards.TowerCardData)cards.cards1 [i + j * 3 + currentpage * 6];
						Vector3 pos = new Vector3 (start.position.x + i * 2, start.position.y - 3 * j, 0);
						if (card.isTower) 
						{
							cardInst = (GameObject)Instantiate (cardTemp, pos, Quaternion.Euler (new Vector3 (0, 0, 0)));	
							cardInst.GetComponent<deckCardInstance> ().SetText ("Damage: " + card.damage.ToString ()
							+ "\nRange: " + card.range + "\nCost: " + card.cost + "\n" + card.description);
						} 
						else 
						{
							cardInst = (GameObject)Instantiate (cardTempAction, pos, Quaternion.Euler (new Vector3 (0, 0, 0)));	
							cardInst.GetComponent<deckCardInstance> ().SetText (card.description + "\nCost: " + card.cost);
						}
						var image = cardInst.transform.FindChild ("image");
						cardInst.GetComponent<deckCardInstance> ().name = card.Name;
						cardInst.GetComponent<deckCardInstance> ().type = card.type;
						image.GetComponent<SpriteRenderer> ().sprite = Resources.Load<Sprite> ("FSM\\" + card.type.ToString ());
						currentCards.Add (cardInst);
					}
				}
			}
		}
	}

	void Update () 
	{
		deckcount.text = "CARDS IN DECK " + cards.PersistentDeck1.Count.ToString () +" /40";
	}

	//Izdēš sobrīd ŗādīto lapu
	void wipeList()
	{
		ArrayList todelete = new ArrayList ();
		foreach (GameObject card in currentCards) 
		{
			todelete.Add (card);
		}
		foreach (GameObject card in todelete) 
		{
			currentCards.Remove (card);
			Destroy (card);
		}


	}

	//Pāriet uz nākamo kāršu lapu
	public void next()
	{
		if ((currentpage +1) * 6 < cards.cards1.Count) 
		{
			currentpage++;
			wipeList ();
			InitializePage ();
		}
	}

	//Pāriet uz iepriekšējo kāršu lapu
	public void previous()
	{
		if (currentpage>0) 
		{
			currentpage--;
			wipeList ();
			InitializePage ();
		}
	}

	//Atpakaļ uz main menu
	public void back()
	{
		SaveLoad.Save (deckinitialize.cardsinDeck);
		Application.LoadLevel (1);
	}
}
