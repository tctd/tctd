﻿using UnityEngine;
using System.Collections;

public class circleScript : MonoBehaviour {
	public SpriteRenderer[] circle1;
	public SpriteRenderer[] circle2;
	int old1;
	int old2;
	//Scripts kas sakārto creepus uz rituāla apļiem
	
	// Ja dzīvību skaits ir mainijies, parādīt creepu pareizajā aplī
	void Update () 
	{
		for (int i = 0; i < 8; i++)
		{
			if(i<8-variables.lives[0])
				circle1[i].enabled = true;
			else
				circle1[i].enabled = false;		

		}
		for (int i = 0; i < 8; i++) 
		{
			if(i<8-variables.lives[1])
				circle2[i].enabled = true;
			else
				circle2[i].enabled = false;		

		}
	}
}
