﻿using UnityEngine;
using System.Collections;

public class deckinitialize : MonoBehaviour {
	static bool starting= true;
	public static ArrayList cardsinDeck = new ArrayList();
	//Loģika, kas izveido 1. spēlētāja kāršu instances. Tika izdalīta deckbuilding veidošanas dēļ
	void Awake () 
	{
		cards.gameDeck1 = new ArrayList ();
		if (starting) 
		{
			//(string Name, string Desc, int Cost, int dmg, int reloadspeed, int Range,int Type)
			SaveLoad.Load();
			cardsinDeck = SaveLoad.list1;
			starting = false;
			cards.cards1.Add (new cards.TowerCardData ("Meatball cannon", "Basic", 3, 2, 2, 6, 0, true));
			cards.cards1.Add (new cards.TowerCardData ("Pasta tower", "Basic 2", 4, 3, 2, 8, 1, true));
			cards.cards1.Add (new cards.TowerCardData ("Bologna sauce tower", "AoE", 4, 2, 1, 6, 2, true));
			cards.cards1.Add (new cards.TowerCardData ("Macaroni shooter", "Sniper", 5, 4, 1, 10, 3, true));
			cards.cards1.Add (new cards.TowerCardData ("Melted cheese canon", "AoE Slow", 4, 0, 1, 6, 4, true));
			cards.cards1.Add (new cards.TowerCardData ("Beer volcano", "Slow", 4, 1, 2, 7, 5, true));
			cards.cards1.Add (new cards.TowerCardData ("Canon", "Damage", 5, 6, 1, 4, 6, true));
			cards.cards1.Add (new cards.TowerCardData ("His holy \nnoodleness shrine", "Percent damage", 4, 1, 3, 6, 7, true));
			cards.cards1.Add (new cards.TowerCardData ("Instant noodles", "Rapid fire canon", 4, 1, 4, 6, 8, true));
			cards.cards1.Add (new cards.TowerCardData ("Blessings of the holy spirits", "Your cultists \nbecome more \ndurable", 1, 0, 0, 0, 9, false));
			cards.cards1.Add (new cards.TowerCardData ("Sacrifice", "His savoury majesty\n transports a\n follower\nto the circle", 3, 0, 0, 0, 10, false));
			cards.cards1.Add (new cards.TowerCardData ("Touched by his noodelieness", "His touch gives\n you haste", 1, 0, 0, 0, 11, false));
			cards.cards1.Add (new cards.TowerCardData ("The first midget", "\"..and on the first\n day he created\n the Earth, Heavens\n and \na midget", 3, 0, 0, 0, 12, false));

			//(string Name, string Desc, int Cost, int dmg, int reloadspeed, int Range,int Type)
			cards.cards2.Add (new cards.TowerCardData ("Black candle", "Basic", 3, 2, 5, 6, 0,true));
			cards.cards2.Add (new cards.TowerCardData ("High priest of Cthulu", "Basic 2", 4, 3, 6, 6, 1,true));
			cards.cards2.Add (new cards.TowerCardData ("Incense of Neptune", "AoE", 4, 3, 6, 6, 2,true));
			cards.cards2.Add (new cards.TowerCardData ("The old one", "Sniper", 4, 3, 6, 6, 3,true));
			cards.cards2.Add (new cards.TowerCardData ("The chalice", "AoE Slow", 4, 3, 6, 6, 4,true));
			cards.cards2.Add (new cards.TowerCardData ("Priest of Cthulu", "Slow", 4, 3, 6, 6, 5,true));
			cards.cards2.Add (new cards.TowerCardData ("The Host", "Damage", 4, 3, 6, 6, 6,true));
			cards.cards2.Add (new cards.TowerCardData ("Shrine of Cthulu", "Percent damage", 4, 3, 6, 6, 7,true));
			cards.cards2.Add (new cards.TowerCardData ("Necronomicon", "Rapid fire canon", 4, 1, 4, 6, 8,true));
			cards.cards2.Add (new cards.TowerCardData ("Witch sabbath", "The holy brew\nstrengthens \nyour followers", 1, 0, 0, 0, 9,false));
			cards.cards2.Add (new cards.TowerCardData ("Call ot Cthulu", "Summon a cultist\n to the ritual\n circle", 3, 0, 0, 0, 10,false));
			cards.cards2.Add (new cards.TowerCardData ("Rite of communion of Cthulu", "Your cultists move\n with unholy\n speed", 1,  0, 0, 0, 11,false));
			cards.cards2.Add (new cards.TowerCardData ("The whisper in darkness", "Sacrifice cultists\n to summon a powerful demon", 3, 0, 0, 0, 12,false));




			InitializeDeck ();
		}



	}

	void InitializeDeck()
		{
			for (int j = 0; j < cardsinDeck.Count; j++) 
			{
				
				
					//cards.TowerCardData card = (cards.TowerCardData)cards.cards1[j];
					for (int i = 0; i < (int)cardsinDeck[j]; i++) 
					{				
						
							//Vector3 pos = new Vector3 (content.position.x, content.position.y- deckbuild.currentCardCount * 12+100, 0);

				cards.PersistentDeck1.Add(cards.cards1[j]);

									
					}

		}
	}


}
