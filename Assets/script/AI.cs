﻿using UnityEngine;
using System.Collections;

public class AI : MonoBehaviour {
	public static bool doMove =  true;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (!variables.WithAI)
			return;
		if (!variables.battlephase && doMove) 
		{
			doMove = false;
			int cycle = Mathf.Min( Random.Range (2, 5),cards.hands[1].Count);
			for (int i = 0; i < cycle; i++) 
			{
				GameObject selectedCard = (GameObject )cards.hands [1] [Random.Range (0, cards.hands [1].Count - 1)];
				cardisntance selectedCardInst = selectedCard.GetComponent<cardisntance> ();
				if (!selectedCardInst.isTower) 
				{
					selectedCardInst.DoAction ();
				} else 
				{
					while (true) {
						int posX = Random.Range (-16, -1);
						int posY = Random.Range (-12, -3);
						var hit = Physics2D.Raycast (new Vector2 (posX, posY), Vector2.zero, 0);
						string tag = "";
						try{
							tag = hit.transform.gameObject.tag;
						}
						catch{
							tag = "";
						}
						if (tag.Equals ("ground2")) {
							variables.mouseInst.green [1].transform.position = new Vector3 (posX, posY, 0);
							variables.selected [1] = selectedCardInst.type;
							variables.selectedCard [1] = selectedCard;
							variables.mouseInst.PlayCard2 ();
							break;
						}
					}

				}

			}
			variables.guiInstance.Go2 ();

		}
	}
}
