﻿using UnityEngine;
using System.Collections;

//Klase kurā atrodas projekta statiskie mainīgie
public class variables : MonoBehaviour {
	public static int[] selected = {-1,-1}; // Izvēlētais tornis katram spēlētājam ( tips)
	public static GameObject[] selectedCard = { null, null }; // Izvēlētā kārts
	public GameObject[] towers; // Visi 1. spēlētāja torņi
	public static GameObject[] towers2; //1. spēlētāja torņu statiskais objekts- lai varētu tikt klāt no citiem scriptiem
	public GameObject[] towersb;// Visi 2. spēlētāja torņi
	public static GameObject[] towers2b;//2. spēlētāja torņu statiskais objekts- lai varētu tikt klāt no citiem scriptiem
	public static bool battlephase = true; 
	public static int[] creepcount = { 0, 0 }; // Spēlē esošo creepu skaits (battlephase laikā)
	public static int[] lives = { 8, 8 }; 
	public static int[] cultistCount= {11,11}; // Pieejamo creepu skaits katram spēlētājam (ko var tērēt building fāzē)
	public static int[] bonusHP={0,0}; //Creepiem papildus pieliekamās dzīvības (No action kārtīm)
	public static int level =1; // Līmenis palielinās pēc katra wave. Nosaka creepu stiprumu
	public static int[] BonusSpeed={0,0};//Creepiem papildus pieliekamā ātrums (No action kārtīm)
	public static int[] cardBeasts = { 0, 0 }; //Papildus ģenerējamie supercreepi (No action kārtīm)
	public static GameObject selectedTower; // Tornis, uz kura ir uzklikšķināts
	public static int cardsadded=4;
	public static bool WithAI = true;
	public static mouse mouseInst;
	public static guiman guiInstance;
	public static singletonClass Singleton;


	void Start () 
	{
		towers2b = towers;
		towers2 = towersb;
		battlephase = true;

	}

	//Ja nospiež "Reset" pauzes menu, šī funkcija atgriež statiskos mainīgos sākuma stāvoklī
	public void Reset()
	{
		battlephase = true;
		for (int i = 0; i < 2; i++) 
		{
			selected[i] = -1;
			selectedCard[i] = null;

			creepcount[i] = 0;
			lives[i] = 8;
			cultistCount[i]= 11;
			bonusHP[i]=0;
			BonusSpeed [i] = 0;
			cardBeasts[i] = 0;
		}
		selectedTower= null;
		level =1;
		battlephase = true;
	}

}
