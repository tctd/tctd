﻿using UnityEngine;
using System.Collections;

public class cardisntance : MonoBehaviour {
	public SpriteRenderer border;
	public int type = 0;
	public int player = 0;
	public int cost =0;
	public SpriteRenderer sp;
	public SpriteRenderer bg;
	public TextMesh Title;
	public TextMesh text;
	public bool selected = false;
	public Vector3 orgPos;
	public Vector3 orgScale;
	public Vector3 shouldPos;
	int sortingpos=0;
	public bool isTower= true;
	public AudioSource source;
	public AudioClip cardActivate;
	public GameObject towerPref = null;
	bool cantime;
	bool canDrag;
	float timeToDrag =0f;

	// Atbild par kārts kas ir spēlētāja rokā uzvedību
	void Start () 
	{
		orgPos = transform.position;
		orgScale = transform.localScale;
	}
	
	// Ielādē kārts attēlu. Izsauc inicailizējot instanci "cards" scriptā
	public void  SetImage()
	{
		if (player == 1)
			sp.sprite = Resources.Load<Sprite> ("FSM\\" + type.ToString ());
		else {	
			if(!variables.WithAI)
				sp.sprite = Resources.Load<Sprite> ("Cthulu\\" + type.ToString ());
		}

	}
	// Maina kārts pozīciju sorting layerā. izmanto "cards" scripts, sakārtojot kārtis rokā( lai pareizi pārklātos
	public void SetOrder(int order)
	{
		orgPos.x = transform.position.x;
		if (variables.WithAI && player == 0)
			return;
		sp.sortingOrder = order - 1;
		bg.sortingOrder = order - 2;
		sortingpos = order;


	}
	public void SetTitle(string title)
	{
		Title.text = title;

	}
	public void SetText(string textt)
	{
		text.text = textt;

	}
	public void DisableBorder()
	{
		border.enabled = false;
	}

	void Update () {
		if( gameObject == mouse.draggedCard)
			//transform.position=new Vector3 (Camera.main.ScreenToWorldPoint (Input.mousePosition).x, Camera.main.ScreenToWorldPoint (Input.mousePosition).y, transform.position.z);
			transform.position = new Vector3(100,100,0);
		if (cantime) {
			timeToDrag += Time.deltaTime;
			if (timeToDrag > 0.075f)
				canDrag = true;
		} else {
			timeToDrag = 0;
			canDrag = false;
		}
		
	}
	void OnMouseUp()
	{
		if (transform.position != orgPos && transform.position != shouldPos) 
		{
			transform.position = orgPos;
			transform.localScale = orgScale;
		}
		canDrag = false;
		cantime = false;
	}
	void OnMouseDown()
	{
		cantime = true;
	}

	void OnMouseOver()
	{
		// Nosaka kas notiek uzklikšķinot uz kārts

		if (Input.GetMouseButton (0) && (!variables.WithAI || player == 1) && isTower && canDrag) 
		{
			Vector3 mousePos = new Vector3 (Camera.main.ScreenToWorldPoint (Input.mousePosition).x, Camera.main.ScreenToWorldPoint (Input.mousePosition).y, transform.position.z);
			transform.position = mousePos;
			if (Vector3.Magnitude (transform.position - orgPos) > 3.5f)
			{
				if(variables.cultistCount [(player + 1) % 2] >= cost && towerPref == null ) 
				{
					//transform.position = new Vector3 (transform.position.x, transform.position.y, -15);				
					mouse.draggedTower= (GameObject)Instantiate(variables.towers2b[type],mousePos,Quaternion.identity);
					mouse.draggedTower.layer = LayerMask.NameToLayer ("Ignore Raycast");
					mouse.draggedCard = gameObject;
					transform.position = new Vector3 (transform.position.x, transform.position.y, -15);
				}else{
					transform.position = orgPos;
					variables.Singleton.pickPlayer ((player+1)%2, "You do not\n have enought creeps");
				}
				
				
		
		} 
		}

		if (Input.GetMouseButtonDown (0) && (!variables.WithAI || player==1)) 
		{
			
			bool oldVal = selected;
			// Apskata visas kārtis spēlētāja rokā
			foreach (GameObject card in cards.hands[(player+1)%2]) 
			{
				cardisntance CScript = card.GetComponent<cardisntance> ();
				// Pārbauda vai uzklikšķinātā kārts jau ir izvēlēta. Doma, lai ar 1 click palielinātu kārti, ar otru izspēlētu
				if (CScript.selected) 
				{
					card.transform.position = CScript.orgPos;
					card.transform.localScale = CScript.orgScale;
					CScript.selected = false;
					if (card.Equals (gameObject) && !isTower) 
					{
						DoAction ();
					}
				}


			}
			//izvēlēšanas bordera loģika
			border.enabled = true;
			border.sortingOrder = sortingpos-2;

			if (oldVal == false) 
			{
				try
				{
				variables.selectedCard [player].GetComponent<cardisntance> ().DisableBorder ();
				}catch{
				}
				selected = true;
				// padara nospiesto kārti par izvēlēto torni atbilstošajam spēlētājam
				if (isTower) 
				{
					variables.selected [(player+1)%2] = type;
					variables.selectedCard [(player+1)%2] = gameObject;
				}
				// Kārts palielināšana, ja tā ir izvēlēta
				transform.localScale = new Vector3 (1.1f, 1.1f, 0.95f);
				Vector3 temp = transform.position;
				if (player == 1)
					temp.y += 1;
				else
					temp.y -= 1;
				shouldPos = temp;
				transform.position = temp;

			}



		}

	}
	//Loģika "Action" ( ne torņu kārtīm)
	public void DoAction()
	{
		if (!variables.battlephase) 
		{
			if (variables.cultistCount [(player + 1) % 2] >= cost) {
				

				if (type == 9)
					variables.bonusHP [(player + 1) % 2] += 1;

				if (type == 10)
					variables.lives [(player + 1) % 2] -= 1;

				if (type == 11)
					variables.BonusSpeed [(player + 1) % 2] += 1;
				if (type == 12)
					variables.cardBeasts [(player + 1) % 2] += 1;

			
				// Iznīcināt kārti pēc izspēlēšanas
				variables.cultistCount [(player + 1) % 2] -= cost;
				guiman.shouldCreepCount = true;
				variables.selected [player] = -1;
				cards.hands [(player + 1) % 2].Remove (gameObject);
				cards.doOrg = true;
				Destroy (gameObject);
				source.PlayOneShot (cardActivate);
			} else 							
				variables.Singleton.pickPlayer ((player + 1) % 2, "You need more cultists to play this");			

		}else
			variables.Singleton.pickPlayer ((player + 1) % 2, "You cannot play cards in the battle phase");	
	}

	}

