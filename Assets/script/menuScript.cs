﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class menuScript : MonoBehaviour 
{
	public Canvas help;
	public Button startText;
	public Button exitText;
	public Button helpStart;
	public Button helpEnd;
	public GameObject MsgBox;
	public Text MsgText;
	//Main menu loģika. Izsauc menu pogas

	void Start ()
	{
		help = help.GetComponent<Canvas> ();
		startText = startText.GetComponent<Button> ();
		exitText = exitText.GetComponent<Button> ();
		helpStart = helpStart.GetComponent<Button> ();
		helpEnd = helpEnd.GetComponent<Button> ();
		help.enabled = false;
		helpEnd.enabled = false;
	}
		
		
	public void HelpPress()
	{
		help.enabled = true;
		startText.enabled = false;
		exitText.enabled = false;
		helpStart.enabled = false;
		helpEnd.enabled = true;
	}

	public void exitPress()
	{
		help.enabled = false;
		startText.enabled = true;
		exitText.enabled = true;
		helpStart.enabled = true;
		helpEnd.enabled = false;

	}

	public void StartLevel ()
	{
		
		if (cards.PersistentDeck1.Count == 40) {
			variables.WithAI = false;
			Application.LoadLevel (2);
		}else {
			MsgText.text = " You need to finish your deck before starting the game";
			MsgBox.SetActive (true);
			StartCoroutine ("closeMSG");
		}

	}
	public void StartLevelAI ()
	{

		if (cards.PersistentDeck1.Count == 40) {
			variables.WithAI = true;
			Application.LoadLevel (2);
		}else {
			MsgText.text = " You need to finish your deck before starting the game";
			MsgBox.SetActive (true);
			StartCoroutine ("closeMSG");
		}

	}
	IEnumerator closeMSG(){
		yield return new WaitForSeconds (2.5f);
		MsgBox.SetActive (false);
	}

	public void DeckBuild ()
	{
		Application.LoadLevel (3); 

	}

	public void ExitGame () 
	{
		Application.Quit();
	}

}