﻿using UnityEngine;
using System.Collections;

public class eyescript : MonoBehaviour {
	Vector3  eyecentre;
	public static Vector3 target;
	float speed =0.8f;
	public Rigidbody2D rb;
	// Loģika cerntālās acs kustināšanai


	void Start () 
	{
		eyecentre =new Vector3 (-8.8f, -11.5f, 0);
		target = eyecentre;

	}

	// Kustināt aci uz mērķi- random pozīciju uz laukuma
	void Update () 
	{
		Vector3 temp = transform.position;
		if (!variables.battlephase)
			speed = 3.5f;
		Vector3 tempo =Vector3.MoveTowards (temp, target, Time.deltaTime * speed);
		rb.MovePosition (new Vector2 (tempo.x, tempo.y));
	}

	// Ja acs zīlīte ietriecas acsābola malā, tad tā nedzudz pagaida un dodās citā virzienā
	void OnCollisionStay2D(Collision2D other)
	{
		if (variables.battlephase) 
		{
			speed = 0;
			target = new Vector3 (Random.Range (-17, 0), Random.Range (-23, 0), 0);
			StartCoroutine ("Wait");
		}
	}
	IEnumerator Wait()
	{
		yield return new WaitForSeconds (0.1f);
		speed = 1.5f;
	}
	// Update is called once per frame

}
