﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class deckCardInstance : MonoBehaviour {
	public TextMesh text;
	public int type;
	public string name;
	public GameObject deckthing; // Objekts, kas parāda cik un kādas kārtis ir deckā
	public Transform startDeckthings;
	public RectTransform content;
	public GameObject MsgBox;
	public Text MsgText;

	//Loģika kārts instancei, kas tiek izmantota deckbildingā

	public void SetText(string textt)
	{
		text.text = textt;

	}
	IEnumerator closeMSG(){
		yield return new WaitForSeconds (2.5f);
		MsgBox.SetActive (false);
	}
	// On clikck ievietot kārti deckā
	void OnMouseOver()
	{
		if (Input.GetMouseButtonDown (0)) {
			if (cards.PersistentDeck1.Count <= 36) {
				for (int i = 0; i < variables.cardsadded; i++) {
					cards.PersistentDeck1.Add (cards.cards1 [type]);
					if ((int)deckinitialize.cardsinDeck [type] == 0) {
						//Vector3 pos = new Vector3 (content.position.x, content.position.y- deckbuild.currentCardCount * 12+100, 0);
						deckbuild.currentCardCount++;
						GameObject cardInst = (GameObject)Instantiate (deckthing, content.position, Quaternion.Euler (new Vector3 (0, 0, 0)));
						cardInst.transform.SetParent (content);
						Vector3 pos = new Vector3 (content.position.x, content.position.y - deckbuild.currentCardCount * 50 + 100, 0);
						content.position = pos;
						//content.sizeDelta =new Vector2(100, 50 * deckbuild.currentCardCount);
						deckbuild.currentCardCount++;
						deckThing thing = cardInst.GetComponent<deckThing> ();
						thing.type = type;
						thing.name = name;


					}

					int temp = (int)deckinitialize.cardsinDeck [type];
					temp++;
					deckinitialize.cardsinDeck [type] = temp;
				}
			} else {			
				MsgText.text = "A deck cannot contain more than 40 cards";
				MsgBox.SetActive (true);
				StartCoroutine ("closeMSG");
			}
		}
	}
}
