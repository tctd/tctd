﻿using UnityEngine;
using System.Collections;
public class pause : MonoBehaviour {
	
	public GameObject pausemenu;
	AudioSource source;
	public AudioClip buttonsound;
	//Pause menu loģika


	void Start () 
	{
		source = GetComponent<AudioSource> ();
	}

	public void Exit()
	{
		source.PlayOneShot (buttonsound);
		Application.LoadLevel (1);
	}
	public void Resume()
	{		
		Time.timeScale = 1f;
		source.PlayOneShot (buttonsound);
		pausemenu.SetActive(false);
	}
	public void Reset()
	{		
		Time.timeScale = 1f;
		source.PlayOneShot (buttonsound);
		Application.LoadLevel (2);
		GetComponent<variables> ().Reset ();
	}

}