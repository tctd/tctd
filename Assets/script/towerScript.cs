﻿using UnityEngine;
using System.Collections;

public class towerScript : MonoBehaviour {
	bool canshoot=true;
	public float reloadtime;
	public GameObject bullet;
	public float bulletspeed = 7;
	public int cost;
	SpriteRenderer sp;
	public Transform bulletPoint;
	public Transform weapon;
	public AudioSource source;
	public AudioClip shootsound;
	float lerped=0;

	// Torņu darbības loģika

	void Start () 
	{
		sp= GetComponent<SpriteRenderer> ();
	}


	void Update () 
	{
		if (transform.parent.gameObject.Equals(variables.selectedTower))
			sp.enabled= true;
		else
			sp.enabled= false;
		if (transform.parent.gameObject == mouse.draggedTower) {
			transform.parent.gameObject.transform.position=new Vector3 (Camera.main.ScreenToWorldPoint (Input.mousePosition).x, Camera.main.ScreenToWorldPoint (Input.mousePosition).y, transform.position.z);
			Debug.Log (transform.position);

		}
	}

	//Šaušanas loģika 
	void OnTriggerStay2D(Collider2D other) 
	{

		if (other.tag == "creep") {
			Vector3 diff = other.transform.position - weapon.position;
			diff.Normalize ();
			float rotz = Mathf.Atan2 (diff.y, diff.x) * Mathf.Rad2Deg- 180;
			float cur = weapon.rotation.eulerAngles.z;
			lerped = Mathf.LerpAngle(rotz,cur,0.9f);
			weapon.rotation = Quaternion.Euler (0, 0, lerped );
		}
		if(canshoot && other.tag =="creep")
		{
			StartCoroutine (Wait());
			canshoot= false;
			//Lodes instancēšana un ātruma piešķiršana tai
			GameObject temp= (GameObject)Instantiate((Object)bullet,bulletPoint.position,Quaternion.identity);
			temp.GetComponent<Rigidbody2D>().velocity = Vector3.Normalize(other.transform.position-transform.position)*bulletspeed;
			temp.GetComponent<bulletScript> ().original = false;
			temp.transform.rotation = Quaternion.Euler (0, 0, lerped );
			//Torņa rotācija
			/*Vector3 diff = other.transform.position- weapon.position;
			diff.Normalize ();
			float rotz = Mathf.Atan2 (diff.y, diff.x) * Mathf.Rad2Deg;
			weapon.rotation = Quaternion.Euler (0, 0, rotz -180);
			temp.transform.rotation = Quaternion.Euler (0, 0, rotz -180);*/
			//Šaušanas skaņa
			source.PlayOneShot (shootsound);
		}
	}

	//Pārlādēšan - lai torņi nešauj visu laiku
	IEnumerator Wait()
	{
		yield return new WaitForSeconds(0.5f/reloadtime);
		canshoot = true;
	}
}
