﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class guiman : MonoBehaviour {
	public Button go1;
	public Button go2;
	public  bool[] GoClicks = { false, false };
	public Text Timer;
	float timeleft = 25;
	public GameObject chtuluWin;
	public GameObject FSMWin;
	public SpriteRenderer[] timericons;
	public SpriteRenderer[] creeptimer1;
	public SpriteRenderer[] creeptimer2;
	public static bool shouldCreepCount = false;
	public SpriteRenderer grid;
	private AudioSource source;
	public AudioClip timerSound;
	public GameObject help;
	public Text creepcount;
	int timerold=8;
	int timerExtraVar =8;
	public GameObject smoke;
	//GUI kontroles loģika. Lielāko daļu no šīm metodēm izsauc pogas


	void Start () 
	{
		variables.guiInstance = this;
		help.SetActive (true);
		Time.timeScale = 0.0f;
		source = GetComponent<AudioSource> ();
		if (variables.WithAI)
			smoke.SetActive (true);
	}

	//Paslēpt sākotnētjo help ekrānu
	public void HideHelp(){
		help.SetActive (false);
		Time.timeScale = 1.0f;
	}
	public void Go1()
	{
		GoClicks [0] = true;
		go1.GetComponent<Image> ().enabled = false;
		if (GoClicks [1])
			timeleft =-1;

	}

	public void Go2()
	{
		GoClicks [1] = true;
		go2.GetComponent<Image> ().enabled = false;
		if (GoClicks [0])
			timeleft =-1;
	}
	//Kontrolē pieejamo creepu rādīšanu uz spēlētāju barakām
	public void SetCreepCount(int player,int count)
	{
		if (player == 0 && variables.WithAI)
			return;
		SpriteRenderer[] temp = creeptimer1;
		if (player == 1)
			temp = creeptimer2;
		for (int i = 0; i < 11; i++) 
		{
			if(i<=count-1)
				temp [i].enabled = true;
			else
				temp [i].enabled = false;		

		}

	}

	void Update () 
	{
		creepcount.text = "Player 1: " + variables.creepcount [0] + " Player2: " + variables.creepcount [1];
		grid.enabled = !variables.battlephase;
		//Nomaina uz barakām redzamo creepu skaitītāju
		if (shouldCreepCount) {
			shouldCreepCount = false;
			SetCreepCount (1, variables.cultistCount [0]);
			SetCreepCount (0, variables.cultistCount [1]);

		}

		//Loģika, lai detektētu vai ir kaujas vai būvēšanas fāze un atbilstoši pārkārtotu laukumu
		if (variables.creepcount [0] == 0 && variables.creepcount [1] == 0 && variables.battlephase) 
		{
			if(variables.WithAI)
				smoke.SetActive (true);
			GoClicks [0] = false;
			GoClicks [1] = false;
			go1.GetComponent<Image> ().enabled = true;
			go2.GetComponent<Image> ().enabled = true;
			variables.battlephase = false;
			variables.cultistCount [0] = 11;
			variables.level++;
			for (int i = 0; i < 2; i++) 
			{
				variables.bonusHP [i] = 0;
				variables.BonusSpeed [i] = 0;
				variables.cardBeasts [i] = 0;
				variables.cultistCount [i] = 11;
				SetCreepCount (i, 11);
			}
			//laika atskaites sākums
			foreach (SpriteRenderer sp in timericons) 
			{
				sp.enabled = true;
			}

		}
		//Uzvaras deklarēšana
		if (variables.lives [0] < 1) 
		{
			FSMWin.SetActive (true);
			Time.timeScale = 0.0f;
		}

		if (variables.lives [1] < 1)
		{
			chtuluWin.SetActive (true);
			Time.timeScale = 0.0f;
		}
		//taimeris
		if (!variables.battlephase) 
		{
			timerold = (int)(timeleft/3);
			timeleft -= Time.deltaTime;
			if ((int)timeleft != timerold * 3 && !source.isPlaying && timerExtraVar != timerold) 
			{
				timerExtraVar = timerold;
				source.PlayOneShot (timerSound);
			}
			for (int i = 0; i < timericons.Length; i++) 
			{
				if (i >= (int)(timeleft/3)) 
				{
					timericons [i].enabled = false;
				} 
				else 
				{
					timericons [i].enabled = true;
				}
					

			}
			//Kaujas palaišana
			if (timeleft <= 2) 
			{
				if( variables.WithAI)
					smoke.SetActive (false);
				GetComponent<cards> ().Draw (2);
				GetComponent<spawner> ().StartSp ();
				timeleft = 25;
				timerold = 24;
				foreach (SpriteRenderer sp in timericons) 
				{
					sp.enabled = false;
				}
			}
		}
	
	}
		}
	

