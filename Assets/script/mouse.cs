﻿using UnityEngine;
using System.Collections;

public class mouse : MonoBehaviour {
	public GameObject[] green;
	string tag="";
	public GameObject pausemenu;
	public AudioClip towerPlace;
	public AudioClip cardSwoosh;
	private AudioSource source;
	public AudioClip Cthuluplace;
	public static GameObject draggedCard;
	public static GameObject draggedTower;
	// Peles cliku apstrāde


	void Awake () {
		source = GetComponent<AudioSource> ();
		variables.mouseInst = this;
	}
	

	void Update () {

		//drag n drop


		try{
			//Raycasts no peles pozīcijas uz laukumu
			var hit = Physics2D.Raycast(new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x,Camera.main.ScreenToWorldPoint(Input.mousePosition).y), Vector2.zero,0);
			try{
				tag = hit.transform.gameObject.tag;
			}
			catch{

			}
			
			//Uzbuvet/ iznicinat viklto torni
			if (Input.GetMouseButtonUp (0)) {
				try{
					if ( draggedCard != null){
						cardisntance CScript = draggedCard.GetComponent<cardisntance> ();
						CScript.towerPref = null;
						variables.cultistCount [(CScript.player + 1) % 2] -= CScript.cost;
						guiman.shouldCreepCount = true;
						if ( tag.Equals("ground")){
							cards.hands [(CScript.player + 1) % 2].Remove (draggedCard);
							Destroy (draggedCard);
							cards.doOrg = true;}
					}
					draggedCard = null;
					if ( !tag.Equals("ground"))
						Destroy (draggedTower);
					draggedTower = null;
					Debug.Log (tag);
				}finally{
					
				}

			}

			//Zaļā kuba novietošana uz laukuma, ja zem peles ir colliders ar "ground" tagu
			if (tag.Equals ("ground") && variables.selected[0]!= -1 && !variables.battlephase)
			{
				green[0].transform.position = new Vector3((int)(Camera.main.ScreenToWorldPoint(Input.mousePosition).x-0.5f),
			(int)(Camera.main.ScreenToWorldPoint(Input.mousePosition).y-0.5f),0);
			}
			else
			{
				green[0].transform.position= new Vector3(40,40,0);
			}
			//Zaļā kuba novietošana uz laukuma, ja zem peles ir colliders ar "ground2" tagu - 2. spēlētājam
			if (tag.Equals ("ground2")&& variables.selected[1]!= -1 && !variables.battlephase)
			{
				green[1].transform.position = new Vector3((int)(Camera.main.ScreenToWorldPoint(Input.mousePosition).x-0.5f),
			(int)(Camera.main.ScreenToWorldPoint(Input.mousePosition).y-0.5f),0);
			}
			else
			{
				green[1].transform.position= new Vector3(40,40,0);
			}
		
		//loģika, ja pele ir nospiesta
		if(Input.GetMouseButtonDown(0))
		{
			//rindiņa, kas kaustina aci būvēšanas laikā
			eyescript.target =Camera.main.ScreenToWorldPoint(Input.mousePosition);
			
			if (hit.transform.gameObject.tag != null) 
			{
				//Pauzēšana uzspežot uz acs
				if(tag.Equals("pause"))
				{
					if (Time.timeScale == 0f) 
					{
						Time.timeScale = 1f;
						pausemenu.SetActive (false);
					} 
					else 
					{
						Time.timeScale = 0f;
						pausemenu.SetActive(true);
					}
				}
				Debug.Log (tag);

				//Torņu būves loģika 1. spēlētājam
					if (tag.Equals ("ground") && variables.selected[0]!= -1)
					{ 
						if(variables.cultistCount[0]>=variables.towers2b[variables.selected[0]].GetComponentInChildren<towerScript>().cost) 
						{
							variables.cultistCount[0]-=variables.towers2b[variables.selected[0]].GetComponentInChildren<towerScript>().cost;
							GetComponent<guiman>().SetCreepCount(1,variables.cultistCount[0]);
							Vector3 temp =Camera.main.ScreenToWorldPoint(Input.mousePosition);
							temp.z =0;
							Instantiate(variables.towers2b[variables.selected[0]],green[0].transform.position,Quaternion.identity);
							//Debug.Log("Selected: " + variables.selected [0]);
							variables.selected [0] = -1;
							cards.hands[0].Remove(variables.selectedCard[0]);
							Destroy(variables.selectedCard[0]);
							source.PlayOneShot(Cthuluplace);
							GetComponent<cards>().organize();
						}else
						{
							if(variables.battlephase)
								variables.Singleton.ShowMessage("You cannot play cards in the battle phase");
							else
								variables.Singleton.ShowMessage("You need more cultists to play this");
						}
					}


				//Torņu būve 2. spēlētājam
					if (tag.Equals ("ground2") && variables.selected[1]!= -1 && !variables.WithAI)
					{
						if(variables.cultistCount[1]>=variables.towers2[variables.selected[1]].GetComponentInChildren<towerScript>().cost) 
						{
							PlayCard2();
						}else
						{
							if(variables.battlephase)
								variables.Singleton.ShowMessage2("You cannot play cards in the battle phase");
							else
								variables.Singleton.ShowMessage2("You need more cultists to play this");
						}
					}


				//skaņa uzspiežot kārti
				if(tag.Equals("card"))
					source.PlayOneShot(cardSwoosh);
			}
			

			}
		}catch{
			//Zaļā kuba pārvietošana ārpus laukuma, ja nav atrasts tags
			green[0].transform.position= new Vector3(40,40,0);
			green[1].transform.position= new Vector3(40,40,0);
		}
	}
	public void PlayCard2()
	{
		if (variables.cultistCount [1] >= variables.towers2 [variables.selected [1]].GetComponentInChildren<towerScript> ().cost) 
		{ 
			variables.cultistCount [1] -= variables.towers2 [variables.selected [1]].GetComponentInChildren<towerScript> ().cost;
			GetComponent<guiman> ().SetCreepCount (0, variables.cultistCount [1]);
			Vector3 temp = Camera.main.ScreenToWorldPoint (Input.mousePosition);
			temp.z = 0;
			Instantiate (variables.towers2 [variables.selected [1]], green [1].transform.position, Quaternion.identity);
			Debug.Log ("Selected: " + variables.selected [1]);
			variables.selected [1] = -1;
			cards.hands [1].Remove (variables.selectedCard [1]);
			Destroy (variables.selectedCard [1]);
			source.PlayOneShot (towerPlace);
			GetComponent<cards> ().organize ();
		}

	}
}
